# NGSH
NGSH is yet another remote shell server, specifically designed to access real Linux terminals from the [OpenComputers Minecraft mod.](https://ocdoc.cil.li/)  
That took me 3 days to write this thing. I'll be expanding it as time goes by.
## Installing and running
The server is written in Go(lang) and the client is written in Lua.
***
There aren't any config files there, so to change the port, change the `Port` constant in the `ngsh-server.go` file to the one you need.  
Then run it: `go build` then `./ngsh`  
***
The client part doesn't depend on anything (except for OpenOS and the internet card of course)  
Run it after launching the server: `ngsh <address> [port]`
## What does NGSH stand for?
No Game's Shell.
## TODO

 - [X] Larger resolution support
 - [ ] Speed optimizations (Slow due to OpenOS's VT100 emulation)
 - [X] UTF-8 (Provided by OC itself)
 - [ ] XTerm compatibility
 - [X] Attributes
 - [X] 16 colours (Provided by OpenOS's VT100 library)
 - [ ] 256 colours
 - [ ] Mouse reporting (limited due to OC's screen capabilities)
 - [ ] Better keyboard handling (most keys work, but not all of them)
 - [X] Cursor :D (salvaged from OpenOS's core libs. Thanks payonel!)
