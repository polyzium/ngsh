package main

import (
	"fmt"
	"net"
	"os"
	"os/exec"
	"regexp"
	"strconv"
	"strings"

	"github.com/kr/pty"
)

const (
	Host  = "0.0.0.0"
	Port  = "5690"
	Shell = "bash"
	Term  = "linux"
)

func main() {
	l, err := net.Listen("tcp", Host+":"+Port)
	if err != nil {
		fmt.Println("NGSH server listening failed: " + err.Error())
		os.Exit(1)
	}
	defer l.Close()
	fmt.Println("Server listening on port " + Port)
	for {
		conn, err := l.Accept()
		if err != nil {
			fmt.Println("Client accept error: " + err.Error())
		}
		go handleClient(conn)
	}
}

func handleClient(conn net.Conn) {
	fmt.Println("Client connected: " + conn.RemoteAddr().String())
	request := make([]byte, 1024)
	_, err := conn.Read(request)
	if err != nil {
		fmt.Println("Client read error: " + err.Error())
		return
	}

	matches, err := regexp.Match("R*x*", request)
	if err != nil {
		fmt.Println("Regex error: " + err.Error())
		return
	} else if !matches {
		fmt.Println("Protocol mismatch from " + conn.RemoteAddr().String())
		conn.Write([]byte("Protocol mismatch"))
		return
	}

	res := strings.Split(strings.ReplaceAll(string(request), "R", ""), "x")
	columns, err := strconv.ParseInt(res[0], 10, 16)
	if err != nil {
		fmt.Println("Parsing error: " + err.Error())
	}
	lines, err := strconv.ParseInt(strings.ReplaceAll(res[1], "\x00", ""), 10, 16) //Freaking nulls. They always annoy me. Unless I find a proper way of getting rid of nulls I'm staying with this
	if err != nil {
		fmt.Println("Parsing error: " + err.Error())
	}
	fmt.Println(strconv.Itoa(int(columns)) + "x" + strconv.Itoa(int(lines)))
	c := exec.Command(Shell)
	c.Env = os.Environ()
	c.Env = append(c.Env, "TERM="+Term)
	f, err := pty.StartWithSize(c, &pty.Winsize{Rows: uint16(lines), Cols: uint16(columns), X: 0, Y: 0})
	if err != nil {
		fmt.Println("Cannot start a pty: " + err.Error())
		return
	}

	//Reader
	go func() {
		for {
			idata := make([]byte, 1)
			_, err = conn.Read(idata)
			if err != nil {
				fmt.Println("Client read error: " + err.Error())
				return
			}
			_, err = f.Write(idata)
			if err != nil {
				fmt.Println("Unable to write to pty: " + err.Error())
				return
			}
		}
	}()

	//Writer
	go func() {
		for {
			pdata := make([]byte, 1)
			_, err = f.Read(pdata)
			if err != nil {
				fmt.Println("Unable to read from pty: " + err.Error() + "; did it exit?")
				return
			}
			_, err = conn.Write(pdata)
			if err != nil {
				fmt.Println("Client send error: " + err.Error())
				return
			}
		}
	}()
}
