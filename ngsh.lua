--[[
NGSH Client
!!! EXPERIMENTAL PTY CLIENT THAT MAY BREAK YOUR SCREEN !!!
https://gitlab.com/polyzium/ngsh
Copyright (C) 2019 Polyzium

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public License
as published by the Free Software Foundation; either version 3
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
]]

--Requires OpenComputers APIs, so use an emulator like OCEmu or OCVM or use the mod itself.

--Libs
--TODO: Remove unused libs
local os = require("os")
local computer = require("computer")
local table = require("table")
local component = require("component")
local inet = component.internet --BUFFERS SUCK! 
local event = require("event")
local shell = require("shell")
local u = require("unicode") --UNICODE YEEE
local term = require("term")
local gpu = component.gpu
local vt100 = require("vt100")
local math = require("math")
local thread = require("thread")
resx,resy = gpu.getResolution()
local altrate = 0 --Alternating rate, because we can't do duplex in OC, so we're forced to simulate it by alternating between read and write
local cursor = require("core/cursor").new()

resx = math.floor(resx)
resy = math.floor(resy)

--Additional VT100 rules by Polyzium
--[?2004[hl]      Bracketed paste, we can't modify OC's paste behaviour so we'll ignore that.
vt100.rules[{"%[", "%?", "2", "0", "0", "4", "[hl]"}] = function(window, ...) end
--]R              Reset palette. Honestly I've never seen programs changing the colour palette. So we'll ignore that too.
vt100.rules[{"%]", "R"}] = function(window, _) end
--c               Reset terminal to its initial state
vt100.rules[{"%c"}] = function(...) --I can't get it to work.
  term.clear()
  term.setCursor(1,1)
  term.window.nowrap = false
end

if not component.isAvailable("internet") then
  io.stderr:write("An internet card is required for running this program.\n")
  os.exit()
end

function split(source, delimiters)
  local elements = {}
  local pattern = '([^'..delimiters..']+)'
  string.gsub(source, pattern, function(value) elements[#elements + 1] =     value;  end);
  return elements
end

local args,ops = shell.parse(...)
if #args < 1 then
  print("Usage: ngsh <address> [port]\nThe default port is 5690.")
  os.exit()
end

if not args[2] then
  args[2] = 5690
end

--Socket
local sock = inet.connect(args[1],args[2]) --Will change the port later

print("Connecting to " .. args[1] .. " on port " .. tostring(args[2]))
sock.finishConnect() --This returns false cuz it isn't connected, but tries to connect
status, reason = sock.finishConnect() --When executing this one more time it returns the actual status

if not status then
  io.stderr:write("Unable to connect, check your internet connection and the server's availability.")
  os.exit()
else
  print("Connected!")
  --print("Screen resolution "..tostring(resx).."x"..tostring(resy))
  sock.write("R".. tostring(resx) .. "x" .. tostring(resy))
end

--Event handling stuff starts here
function unknownEvent() --Dummy for other events
end

local myEventHandlers = setmetatable({}, { __index = function() return unknownEvent end })

function myEventHandlers.key_down(char, code, playerName)
  if code == 200 then --Up arrow
    sock.write("\x1b[A")
  elseif code == 208 then --Down arrow
    sock.write("\x1b[B")
  elseif code == 203 then --Left arrow
    sock.write("\x1b[D")
  elseif code == 205 then --Right arrow
    sock.write("\x1b[C")
  else
    sock.write(u.char(char)) --Continue sending normal keypresses, with Unicode support
  end
end

function handleEvent(eventID, ...)
  if (eventID) then
    myEventHandlers[eventID](...)
  end
end
--Event handling stuff ends here

function blinkCursor(sec) --It isn't so convenient to use the terminal without a cursor. Thanks payonel for the snippet!
  cursor:echo()
  local pack = table.pack(computer.pullSignal(term.window.blink and sec or math.huge))
  local name = pack[1]
  cursor:echo(not name)
  return table.unpack(pack)
end

--TODO: Replace this with a difference updater
function readAndWrapScreen()
  screen = sock.read()
  --gpu.set(1,50,"DEBUG: " .. tostring(screen))
  if not screen then
    print("\nConnection lost")
    os.exit()
  --elseif screen:find("\r\nexit\r\n") then
  elseif screen == "\r\nexit\r\n" then
    print("\nShell exited")
    os.exit()
  elseif screen ~= "" then
    term.write(screen)
  else
    --do nothing
  end
end

--Main thread
while true do
  readAndWrapScreen()
  term.read( { handle = function(self, ...) 
    handleEvent(...) 
    return false
  end } )
end